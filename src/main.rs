extern crate sdl2;
#[macro_use]
extern crate log;
extern crate bytes;
extern crate env_logger;

use std::sync::atomic::{AtomicBool, Ordering};

use bytes::{BufMut, BytesMut};
use sdl2::pixels::PixelFormatEnum;
use sdl2::render::{Texture, TextureAccess, TextureCreator, TextureValueError, WindowCanvas};

pub const RUNNING: AtomicBool = AtomicBool::new(false);
const BYTES_PER_PIXEL: i32 = 4;

fn render_weird_gradient(
    texture: &mut Texture,
    height: u32,
    width: u32,
    green_offset: u8,
    blue_offset: u8,
) {
    let mut temp_pixel_vec =
        BytesMut::with_capacity((height * width * BYTES_PER_PIXEL as u32) as usize);

    trace!("Filling buffer with data");
    for col in 0..height {
        for lin in 0..width {
            //this is blue
            temp_pixel_vec.put_u8(col as u8 + blue_offset);
            //ŧhis is green
            temp_pixel_vec.put_u8(lin as u8 + green_offset);
            //ŧhis is red
            temp_pixel_vec.put_u8(0);
            //ŧhis isalpha
            temp_pixel_vec.put_u8(0);
        }
    }

    trace!("Updating texture");
    texture.update(
        None,
        temp_pixel_vec.as_ref(),
        (width * BYTES_PER_PIXEL as u32) as usize,
    );
}

fn resize_texture<T>(
    texture_creator: &TextureCreator<T>,
    height: u32,
    width: u32,
) -> Result<Texture, TextureValueError> {
    trace!("resize_texture");
    texture_creator.create_texture(
        PixelFormatEnum::ARGB8888,
        TextureAccess::Streaming,
        width,
        height,
    )
}

fn update_window(canvas: &mut WindowCanvas, texture: &mut Texture, _pixel_data: &[u8]) {
    trace!("update_window");
    let win_size = canvas.window().size();
    render_weird_gradient(texture, win_size.0, win_size.1, 0, 0);
    canvas.copy(texture, None, None);
    canvas.present();
}

fn main() {
    env_logger::init();
    debug!("Starting sdl context");
    let sdl_context = sdl2::init().expect("Error initializing sdl2");
    debug!("Starting video subsystem");
    let video_subsystem = sdl_context
        .video()
        .expect("Error initializing video subsystem");
    debug!("Setting up window");
    let window = video_subsystem
        .window("rustmade-hero", 800, 600)
        .resizable()
        .position_centered()
        .build()
        .expect("Error openning window");

    debug!("Preparing canvas");
    let mut canvas = window.into_canvas().build().unwrap();
    let mut initial_window_size = canvas.window().size();
    canvas.clear();
    canvas.present();

    let mut event_pump = sdl_context.event_pump().unwrap();

    RUNNING.store(true, Ordering::Release);

    println!(
        "Initial window size: {} x {} ",
        initial_window_size.0, initial_window_size.1
    );

    let mut pixels = BytesMut::with_capacity(
        (initial_window_size.0 * initial_window_size.1 * BYTES_PER_PIXEL as u32) as usize,
    );
    let create = canvas.texture_creator();
    let mut texture = resize_texture(&create, initial_window_size.0, initial_window_size.1)
        .expect("Error resizing texture");

    update_window(&mut canvas, &mut texture, pixels.as_ref());
    debug!("Starting event pump");
    use sdl2::pixels::Color;
    let mut cur_color = Color::RGB(255, 255, 255);
    'running: loop {
        for event in event_pump.poll_iter() {
            use sdl2::event::{Event, WindowEvent};
            match event {
                Event::Quit { .. } => {
                    RUNNING.store(false, Ordering::Release);
                    debug!("Quit event");
                    break 'running;
                }
                Event::Window { win_event, .. } => match win_event {
                    WindowEvent::Close => {
                        RUNNING.store(false, Ordering::Relaxed);
                        debug!("Window close event");
                        break 'running;
                    }
                    WindowEvent::SizeChanged(height, width) => {
                        debug!("SizeChanged event with ({}, {})", height, width);
                        texture = resize_texture(&create, height as u32, width as u32)
                            .expect("Error resizing texture");
                        pixels.resize((width * height * BYTES_PER_PIXEL) as usize, 0u8);
                    }
                    WindowEvent::Exposed => {
                        debug!("Window event exposed");
                        update_window(&mut canvas, &mut texture, pixels.as_ref());
                    }
                    _ => (),
                },
                _ => (),
            }
        }
    }
}
